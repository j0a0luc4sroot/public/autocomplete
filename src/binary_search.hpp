#ifndef BINARY_SEARCH_HPP
#define BINARY_SEARCH_HPP

#include "autocomplete.hpp"
#include "utils.hpp"
#include <algorithm>
#include <string>
#include <vector>

class BinarySearch : public AutoComplete {
public:
  BinarySearch(const std::vector<std::string> &);
  std::vector<std::string> complete(std::string, const std::size_t &);

private:
  std::vector<std::string> strings;
};

#endif // !BINARY_SEARCH_HPP
