#include "automaton_state.hpp"

AutomatonState::Ptr AutomatonState::create() {
  return Ptr(new AutomatonState());
}

AutomatonState::Ptr AutomatonState::create(const Ptr &other) {
  return Ptr(new AutomatonState(*other));
}

void AutomatonState::renew() {
  uuid = uuid_generator();
  transitions.clear();
  is_final = false;
}

bool AutomatonState::operator==(const AutomatonState &other) {
  return transitions.size() == other.transitions.size() and
         is_final == other.is_final and transitions == other.transitions;
}

bool AutomatonState::operator==(const AutomatonState &other) const {
  return transitions.size() == other.transitions.size() and
         is_final == other.is_final and transitions == other.transitions;
}

void AutomatonState::set_transition(char symbol, const Ptr &next) {
  transitions[symbol] = next;
}

AutomatonState::Ptr AutomatonState::get_transition(char symbol) {
  TransitionMap::iterator transitions_it;

  transitions_it = transitions.find(symbol);
  if (transitions_it == transitions.end())
    return nullptr;
  return transitions_it->second;
}

const AutomatonState::TransitionMap &AutomatonState::get_transitions() {
  return transitions;
}

void AutomatonState::set_is_final(bool is_final) {
  this->is_final = is_final;
}

bool AutomatonState::get_is_final() {
  return is_final;
}

std::size_t AutomatonState::Hash::operator()(const AutomatonState &automaton_state) const noexcept {
  std::size_t hash;

  hash = 0;

  for (const auto &[symbol, next_state] : automaton_state.transitions)
    hash += (symbol + hash_uuid(next_state->uuid)) * TRANSITION_MAGIC;

  return hash;
}

std::size_t AutomatonState::Hash::operator()(const Ptr &automaton_state_ptr) const noexcept {
  return operator()(*automaton_state_ptr);
}

bool AutomatonState::EqualTo::operator()(const AutomatonState &lhs, const AutomatonState &rhs) const noexcept {
  return (lhs == rhs);
}

bool AutomatonState::EqualTo::operator()(const Ptr &lhs, const Ptr &rhs) const noexcept {
  return operator()(*lhs, *rhs);
}

AutomatonState::AutomatonState() {
  uuid = uuid_generator();
  is_final = false;
}

AutomatonState::AutomatonState(const AutomatonState &other) {
  uuid = other.uuid;
  transitions = other.transitions;
  is_final = other.is_final;
}

boost::hash<boost::uuids::uuid> AutomatonState::hash_uuid;
boost::uuids::random_generator AutomatonState::uuid_generator;
