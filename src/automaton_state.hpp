#ifndef AUTOMATON_STATE_HPP
#define AUTOMATON_STATE_HPP

#include <boost/functional/hash.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid.hpp>
#include <map>
#include <memory>
#include <stack>
#include <unordered_set>

#define TRANSITION_MAGIC 1001

class AutomatonState;

class AutomatonState {
public:
  using Ptr = std::shared_ptr<AutomatonState>;
  using TransitionMap = std::map<char, Ptr>;

  static Ptr create();
  static Ptr create(const Ptr &);
  void renew();

  bool operator==(const AutomatonState &);
  bool operator==(const AutomatonState &) const;

  void set_transition(char, const Ptr &);
  Ptr get_transition(char);
  const TransitionMap &get_transitions();

  void set_is_final(bool);
  bool get_is_final();

  struct Hash {
    std::size_t operator()(const AutomatonState &) const noexcept;
    std::size_t operator()(const Ptr &) const noexcept;
  };

  struct EqualTo {
    bool operator()(const AutomatonState &,
                    const AutomatonState &) const noexcept;
    bool operator()(const Ptr &, const Ptr &) const noexcept;
  };

private:
  AutomatonState();
  AutomatonState(const AutomatonState &);

  boost::uuids::uuid uuid;
  TransitionMap transitions;
  bool is_final;

  static boost::hash<boost::uuids::uuid> hash_uuid;
  static boost::uuids::random_generator uuid_generator;

public:
  using Set = std::unordered_set<Ptr, Hash, EqualTo>;
  using Stack = std::stack<Ptr>;
  using Vector = std::vector<Ptr>;
};

#endif // !AUTOMATON_STATE_HPP
