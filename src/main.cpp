#include <chrono>
#include <fstream>
#include <ncurses.h>
#include <string>
#include <vector>

#include "binary_search.hpp"
#include "fst.hpp"
#include "tests.hpp"
#include "utils.hpp"

int main() {
  std::string prefix;
  std::vector<std::string> strings;
  std::vector<std::string> completed_strings_bs;
  std::vector<std::string> completed_strings_fst;
  std::vector<std::string> completed_strings_fst_lev;
  int i;
  int ch;
  const std::vector<char> alphabet = get_alphabet_hard_coded();

  initscr();
  raw();
  keypad(stdscr, TRUE);
  noecho();
  printw("Loading");
  refresh();
  strings = get_strings("../data/american-english");
  BinarySearch bs(strings);
  FiniteStateTransducer fst(strings);
  move(0, 0);
  clrtoeol();
  refresh();
  while ((ch = getch())) {
    if (ch == '0') {
      break;
    } else if (ch == KEY_BACKSPACE) {
      if (!prefix.empty()) {
        prefix.pop_back();
        mvaddch(0, prefix.size(), ' ');
      }
    } else {
      mvaddch(0, prefix.size(), ch);
      prefix.push_back(ch);
    }
    auto start_bs = std::chrono::high_resolution_clock::now();
    completed_strings_bs = bs.complete(prefix, 10);
    auto stop_bs = std::chrono::high_resolution_clock::now();
    auto duration_bs = std::chrono::duration_cast<std::chrono::microseconds>(stop_bs - start_bs);

    auto start_fst = std::chrono::high_resolution_clock::now();
    completed_strings_fst = fst.complete(prefix,10);
    auto stop_fst = std::chrono::high_resolution_clock::now();
    auto duration_fst = std::chrono::duration_cast<std::chrono::microseconds>(stop_fst - start_fst);

    auto start_fst_lev = std::chrono::high_resolution_clock::now();
    completed_strings_fst_lev = fst.complete_levenshtein(prefix, -1, 1, alphabet);
    auto stop_fst_lev = std::chrono::high_resolution_clock::now();
    auto duration_fst_lev = std::chrono::duration_cast<std::chrono::microseconds>(stop_fst_lev - start_fst_lev);

    for (i = 2; i < 15; i++) {
      move(i, 0);
      clrtoeol();
    }
    if (not prefix.empty()) {
      for (i = 0; i < completed_strings_bs.size(); i++) {
        move(i + 2, 0);
        printw("%s", completed_strings_bs[i].c_str());
      }
      for (i = 0; i < completed_strings_fst.size(); i++) {
        move(i + 2, 25);
        printw("%s", completed_strings_fst[i].c_str());
      }
      for (i = 0; i < completed_strings_fst_lev.size(); i++) {
        move(i + 2, 50);
        printw("%s", completed_strings_fst_lev[i].c_str());
      }
    }
    move(18, 0);
    printw("Time taken by Binary Search: %ld microsseconds", duration_bs.count());
    move(19, 0);
    printw("Time taken by FST: %ld microsseconds", duration_fst.count());
    move(20, 0);
    printw("Time taken by FST with Levenshtein: %ld microsseconds", duration_fst_lev.count());

    move(0, prefix.size());
    refresh();
  }
  endwin();
}
