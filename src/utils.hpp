#ifndef UTILS_HPP
#define UTILS_HPP

#include <algorithm>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

std::vector<std::string> get_strings(const std::string &);
std::string find_common_prefix(const std::string &, const std::string &);

#endif
