#include "utils.hpp"

std::vector<std::string> get_strings(const std::string &path) {
  std::vector<std::string> strings;
  std::string string;

  std::ifstream file(path);
  while (std::getline(file, string))
    strings.push_back(string);

  return strings;
}

std::string find_common_prefix(const std::string &lhs, const std::string &rhs) {
  std::size_t n;
  std::size_t i;

  n = std::min(lhs.size(), rhs.size());
  for (i = 0; i < n && lhs[i] == rhs[i]; i++);
  return lhs.substr(0, i);
}
