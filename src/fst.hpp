#ifndef FST_HPP
#define FST_HPP

#include <string>
#include <vector>

#include "autocomplete.hpp"
#include "automaton_state.hpp"
#include "utils.hpp"

class FiniteStateTransducer : public AutoComplete {
public:
  FiniteStateTransducer(const std::vector<std::string> &);
  std::vector<std::string> complete(std::string, const std::size_t &);

protected:
  AutomatonState::Ptr find_minimized_state(AutomatonState::Set &, const AutomatonState::Ptr &);
  void complete_recursive(std::vector<std::string> &, std::string &, const std::size_t &, const AutomatonState::Ptr &);

private:
  AutomatonState::Ptr initial_state;
};

#endif // !FST_HPP
