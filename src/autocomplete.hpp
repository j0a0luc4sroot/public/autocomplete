#ifndef AUTOCOMPLETE_HPP
#define AUTOCOMPLETE_HPP

#include <string>
#include <vector>

class AutoComplete {
public:
  virtual std::vector<std::string> complete(std::string, const std::size_t &) = 0;
};

#endif // !AUTOCOMPLETE_HPP
