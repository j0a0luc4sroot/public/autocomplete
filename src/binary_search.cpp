#include "binary_search.hpp"

BinarySearch::BinarySearch(const std::vector<std::string> &strings) {
  this->strings = strings;
}

std::vector<std::string> BinarySearch::complete(std::string prefix, const std::size_t &n) {
  std::vector<std::string> completed_strings;
  std::vector<std::string>::const_iterator strings_it;

  for (strings_it = std::lower_bound(strings.begin(), strings.end(), prefix);
       strings_it != strings.end() && prefix == find_common_prefix(*strings_it, prefix) && completed_strings.size() < n;
       strings_it++)
    completed_strings.push_back(*strings_it);

  return completed_strings;
}
