#include "fst.hpp"

FiniteStateTransducer::FiniteStateTransducer(const std::vector<std::string> &strings) {
  std::size_t max_word_size;

  std::size_t i;

  std::string previous_string;
  std::string prefix;

  AutomatonState::Set states;
  AutomatonState::Vector temp_states;

  max_word_size = std::max_element(strings.begin(), strings.end(),
                       [](const std::string &lhs, const std::string &rhs) {
                         return lhs.size() < rhs.size();
                       })->size();

  for (i = 0; i <= max_word_size; i++)
    temp_states.push_back(AutomatonState::create());

  previous_string = "";
  for (const std::string &string : strings) {
    prefix = find_common_prefix(previous_string, string);
    for (i = previous_string.size(); i > prefix.size(); i--)
      temp_states[i - 1]->set_transition(previous_string[i - 1], find_minimized_state(states, temp_states[i]));
    for (i = prefix.size(); i < string.size(); i++) {
      temp_states[i + 1]->renew();
      temp_states[i]->set_transition(string[i], temp_states[i + 1]);
    }
    if (string != previous_string)
      temp_states[string.size()]->set_is_final(true);
    previous_string = string;
  }

  for (i = strings.back().size(); i > 0; i--)
    temp_states[i - 1]->set_transition(strings.back()[i - 1], find_minimized_state(states, temp_states[i]));
  initial_state = find_minimized_state(states, temp_states[0]);
}

std::vector<std::string> FiniteStateTransducer::complete(std::string prefix, const std::size_t &n) {
  std::vector<std::string> completed_strings;
  AutomatonState::Ptr state;

  state = initial_state;
  for (const char &symbol : prefix) {
    state = state->get_transition(symbol);
    if (not state)
      return completed_strings;
  }

  complete_recursive(completed_strings, prefix, n, state);

  return completed_strings;
}

AutomatonState::Ptr FiniteStateTransducer::find_minimized_state(AutomatonState::Set &states, const AutomatonState::Ptr &state) {
  AutomatonState::Set::const_iterator states_it;
  AutomatonState::Ptr new_state;

  states_it = states.find(state);
  if (states_it == states.end()) {
    new_state = AutomatonState::create(state);
    states.insert(new_state);
    return new_state;
  }

  return *states_it;
}

void FiniteStateTransducer::complete_recursive(std::vector<std::string> &completed_strings, std::string &string, const std::size_t &n, const AutomatonState::Ptr &state) {
  if (completed_strings.size() == n)
    return;
  if (state->get_is_final())
    completed_strings.push_back(string);
  for (const auto &[symbol, next_state] : state->get_transitions()) {
    string += symbol;
    complete_recursive(completed_strings, string, n, next_state);
    string.pop_back();
  };
}
